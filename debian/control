Source: sslsplit
Section: net
Priority: optional
Maintainer: Hilko Bengen <bengen@debian.org>
Build-Depends: debhelper-compat (= 13), libssl-dev, libevent-dev, check, pkg-config, openssl,
 libnet1-dev, libpcap-dev,
Standards-Version: 4.2.1
Homepage: https://www.roe.ch/SSLsplit
Vcs-Git: https://salsa.debian.org/debian/sslsplit.git
Vcs-Browser: https://salsa.debian.org/debian/sslsplit

Package: sslsplit
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: transparent and scalable SSL/TLS interception
 SSLsplit is a tool for man-in-the-middle attacks against SSL/TLS
 encrypted network connections. Connections are transparently
 intercepted through a network address translation engine and
 redirected to SSLsplit. SSLsplit terminates SSL/TLS and initiates a
 new SSL/TLS connection to the original destination address, while
 logging all data transmitted. SSLsplit is intended to be useful for
 network forensics and penetration testing.
